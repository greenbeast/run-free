#!/usr/bin/bash

# pulled from https://github.com/joanisc/loyaltyCardsOpen/blob/master/install.sh
# which is a project I contribute to

path="${HOME}/.local/share"

mkdir -p "$path/run-free"

if [ $? -ne 0 ]; then 
	echo ""
	echo "Could not create folder '$path/run-free'"
	exit
fi

echo "" 
echo "Copying download folder to $path/run-free"
cp -ru ./* "$path/run-free"

if [ $? -ne 0 ]; then
	echo "" 
	echo "Could not copy download folder to $path/run-free"
	exit
fi

echo "" 
echo "Create a new desktop file"
rm -f "$path/applications/run-free.desktop"

echo "[Desktop Entry]
Name=Run Free!
X-GNOME-FullName=Run Free!
Comment=A running log built for Phosh and tested on Mobian
Keywords=Running; Exercise;
Exec="$path/run-free/run_free.sh"
Terminal=false
StartupNotify=true
Type=Application
Icon=$path/run-free/Icons/run.svg
Categories=GNOME;GTK;Utility;
MimeType=application/python;
Name[en_US]=Run Free!
X-Purism-FormFactor=Workstation;Mobile;" > "$path/applications/run-free.desktop"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create desktop file in to $path/applications"
	exit
fi

rm -f "$path/run_free.sh"

echo "#!/usr/bin/bash
cd '$path/run-free'
python3 run_free.py" > "$path/run-free/run_free.sh"

chmod 0755 "$path/run-free/run_free.sh"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create shell script in to $path/run-free"
	exit
fi

echo "" 
echo "Install complete - you may (optionally) remove the download folder"

import matplotlib
matplotlib.use('GTK3Agg')  # or 'GTK3Cairo'
import matplotlib.pyplot as plt
import numpy as np

import sqlite3
from datetime import datetime, timedelta

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

conn = sqlite3.connect("runs.sqlite")
cur = conn.cursor()

table = "RUNS"

sql = f"select * from {table}"

cur.execute(sql)

records = cur.fetchall()

type_list = []
runs = []
for record in records:
    runs.append(record)


today = datetime.now()
last_week = today + timedelta(weeks=-1)
last_week = last_week.strftime("%m/%d/%Y")
last_week = str(last_week)

last_week = last_week[1:]

for run in runs:
    if run[0] < last_week:
        pass
    elif run[0] > last_week:
        type_list.append(run[4])

num_hill = 0
num_intervals = 0
num_standard = 0
num_tempo = 0
        
for run in type_list:
    if run == "Hill":
        num_hill += 1
        
    elif run == "Tempo":
        num_tempo += 1

    elif run == "Interval":
        num_intervals += 1

    elif run == "Standard":
        num_standard += 1


df = np.array([num_standard, num_tempo, num_intervals, num_hill])
        
        
label = ["Regular", "Tempo", "Interval", "Hill"]

fig, ax = plt.subplots(figsize=(2.5,4.3))
plt.xticks(rotation=90)

ax.bar(label, df)
#ax.legend()
fig.tight_layout()
manager = fig.canvas.manager
vbox = manager.vbox
#plt.figure(figsize=(2.5,4))
plt.show()

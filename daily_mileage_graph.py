import matplotlib
matplotlib.use('GTK3Agg')  # or 'GTK3Cairo'
import matplotlib.pyplot as plt
import numpy as np

import sqlite3
from datetime import datetime, timedelta

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

conn = sqlite3.connect("runs.sqlite")
cur = conn.cursor()

table = "RUNS"

sql = f"select * from {table}"

cur.execute(sql)

records = cur.fetchall()

distance_list = []
date_list = []
runs = []
for record in records:
    runs.append(record)

dates = []

today = datetime.now()
last_week = today + timedelta(weeks=-1)
last_week = last_week.strftime("%m/%d/%Y")
last_week = str(last_week)

last_week = last_week[1:]

for run in runs:
    if run[0] < last_week:
        pass
    elif run[0] > last_week:
        date_list.append(run[0])
        distance_list.append(run[1])
#print(date_list, distance_list)

mean = [np.mean(distance_list)]*len(date_list)


fig, ax = plt.subplots(figsize=(2.5,4.3))
plt.xticks(rotation=90)

#fig.right_layout()

ax.plot(date_list, distance_list, label="Daily Mileage", marker="*", linestyle=":")
mean_line = ax.plot(date_list, mean, label=f'Mean({round(mean[0],1)} miles)', linestyle='--')
fig.tight_layout()

ax.legend(loc="upper left")


manager = fig.canvas.manager
vbox = manager.vbox

# now let's add a widget to the vbox


plt.show()

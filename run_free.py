#!/usr/bin/python3
import gi
import os
import sys
import subprocess as sp
import sqlite3
import random
from datetime import datetime, timedelta



conn = sqlite3.connect("runs.sqlite")
cur = conn.cursor()

table = "RUNS"

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk, Gio, Gdk, GLib


class ListBoxRowWithData(Gtk.ListBoxRow):
    def __init__(self, data):
        super().__init__()
        self.data = data
        self.add(Gtk.Label(label=data))

# Class that pulls up the calendar dialog screen when setting reminders
class CalDialog(Gtk.Dialog):
    """
    Calendar Dialog
    """

    def __init__(self, parent):
        Gtk.Dialog.__init__(
            self,
            "Select Date",
            parent,
            0,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK,
                Gtk.ResponseType.OK,
            ),
        )

        self.set_default_size(300, 200)

        self.value = None

        box = self.get_content_area()

        calendar = Gtk.Calendar()
        calendar.set_detail_height_rows(1)
        calendar.set_property("show-details", True)
        calendar.set_detail_func(self.cal_entry)

        box.add(calendar)

        self.show_all()

    def cal_entry(self, calendar, year, month, date):
        self.value = calendar.get_date()


class running_calendar(Gtk.Window):
    """
    Basic running log app for Mobian (maybe all phosh?)
    EXPAND HERE
    """

    def __init__(self):
        Gtk.Window.__init__(self, title="Run Free!")

        self.set_border_width(0)
        self.set_default_size(200, 400)  # Seems close to the right size

        # date stuff
        self.today = datetime.now()
        self.last_week = self.today + timedelta(weeks=-1)
        self.last_week = self.last_week.strftime("%m/%d/%Y")
        self.last_week = str(self.last_week)

        self.two_weeks = self.today + timedelta(weeks=-2)
        self.two_weeks = self.two_weeks.strftime("%m/%d/%Y")
        self.two_weeks = str(self.two_weeks)

        # Notebook makes the screen tabbed
        self.notebook = Gtk.Notebook()
        self.notebook.set_vexpand(True)
        self.notebook.set_hexpand(True)
        self.add(self.notebook)

        # Intro page info!
        self.intro_page = Gtk.ScrolledWindow()
        self.intro_page.set_border_width(5)

        vscrollbar = Gtk.Scrollbar(orientation=Gtk.Orientation.VERTICAL)

        """
        Add a comboboxtext to choose from week or month and then show the user all of those runs
        as well.
        and something like if text==month def get_monthly_runs elif text==week def read_from_db
        but I would likely need to change the name of the function
        """

        self.intro_label = Gtk.Label(label="This weeks runs:\n")
        self.intro_label.props.halign = Gtk.Align.CENTER
        self.listbox_2 = Gtk.ListBox()

        # reads from the calendar to get runs from the last week
        items = self.read_from_db()
        
        if items is None:
            items = []
        else:
            items = items
            
        self.listbox_2.add(self.intro_label)

        x = 0
        # lists through the items pulled and adds them to the
        # list of reminders displayed
        val = 0
        for item in items:
            self.listbox_2.add(ListBoxRowWithData(" ".join([str(x) for x in item])))
            self.listbox_2.add(ListBoxRowWithData('\n'))
            

        def on_row_activated(listbox_widget, row):
            print(row.data)

        self.listbox_2.connect("row-activated", on_row_activated)

        self.intro_page.add(self.listbox_2)
        self.listbox_2.show_all()

        self.notebook.append_page(self.intro_page, Gtk.Label(label="Runs"))# need a better name
        
        # Reminder page info!
        self.run_page = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.run_page)

        # There are a few different list boxes here so that everything can
        # be added to the same screen without having to worry about sizing
        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.run_page.pack_start(listbox, True, True, 0)


        row = Gtk.ListBoxRow()
        grid = Gtk.Grid(orientation=Gtk.Orientation.HORIZONTAL)
        row.add(grid)
        grid.set_hexpand(True)
        grid.set_border_width(3)
        
        # calendar button and icon
        calendar_button = Gtk.Button()
        calendar_image = Gtk.Image()
        calendar_image.set_from_file(os.path.join(sys.path[0], "Icons", "calendar.svg"))
        calendar_button.add(calendar_image)
        calendar_button.connect("clicked", self.calendar_entry)
        calendar_button.set_hexpand(True)
        
        # date stuff that populates the date entry with the current date and time
        year = datetime.now().year
        month = datetime.now().month
        day = datetime.now().day
        hour = datetime.now().strftime("%H")
        minute = datetime.now().strftime("%M")
        
        # date entry
        self.user_entry = Gtk.Entry()
        self.user_entry.set_text(f"{month}/{day}/{year}")
        self.user_entry.set_hexpand(True)

        # attach(item_name, column, row, col_span, row_span)
        grid.attach(calendar_button, 2, 0, 1, 1)
        grid.attach(self.user_entry, 4, 0, 1, 1)
        listbox.add(row)

        row = Gtk.ListBoxRow()
        grid = Gtk.Grid(orientation=Gtk.Orientation.HORIZONTAL)
        row.add(grid)
        grid.set_hexpand(True)
        grid.set_border_width(3)

        adjustments = Gtk.Adjustment(upper=99, step_increment=1)
        self.distance_spin1 = Gtk.SpinButton(orientation=Gtk.Orientation.VERTICAL)
        self.distance_spin1.set_adjustment(adjustments)

        adjustments = Gtk.Adjustment(upper=9, lower=00, step_increment=1)
        self.distance_spin2 = Gtk.SpinButton(orientation=Gtk.Orientation.VERTICAL)
        self.distance_spin2.set_adjustment(adjustments)
        self.distance_spin2.set_value(00)
        
        distance_label = Gtk.Label(label="Distance:\t")
        distance_label.props.halign = Gtk.Align.CENTER

        distance_decimal = Gtk.Label(label=".")

        # would eventually like to add a dropdown to choose miles or km
        miles_label = Gtk.Label(label="Miles")
        
        # attach(item_name, column, row, col_span, row_span)
        grid.attach(distance_label, 0, 0, 1, 1)
        grid.attach(self.distance_spin1, 2, 0, 1, 1)
        grid.attach(distance_decimal, 3, 0, 1, 1)
        grid.attach(self.distance_spin2, 4, 0, 1, 1)
        grid.attach(miles_label, 5, 0, 1, 1)
        listbox.add(row)


        row = Gtk.ListBoxRow()
        grid = Gtk.Grid(orientation=Gtk.Orientation.HORIZONTAL)
        row.add(grid)
        grid.set_hexpand(True)
        grid.set_border_width(3)

        # need spinner box for distance/pace and combo box for effort/type
        # set up like distance_spin1.distance_spin2 miles, i.e 1.6 miles
        adjustments = Gtk.Adjustment(upper=59, step_increment=1)
        self.minute_spin = Gtk.SpinButton(orientation=Gtk.Orientation.VERTICAL)
        self.minute_spin.set_adjustment(adjustments)

        adjustments = Gtk.Adjustment(upper=59, lower=00, step_increment=5)
        self.second_spin = Gtk.SpinButton(orientation=Gtk.Orientation.VERTICAL)
        self.second_spin.set_adjustment(adjustments)
        
        pace_label = Gtk.Label(label="Pace:\t\t")
        pace_label.props.halign = Gtk.Align.CENTER

        # would like to add a combo box for min/km
        min_mile = Gtk.Label(label="min/mile")
        pace_semicolon = Gtk.Label(label=":")
        
        # attach(item_name, column, row, col_span, row_span)
        grid.attach(pace_label, 0, 0, 1, 1)
        grid.attach(self.minute_spin, 2, 0, 1, 1)
        grid.attach(pace_semicolon, 3, 0, 1, 1)
        grid.attach(self.second_spin, 4, 0, 1, 1)
        grid.attach(min_mile, 5, 0, 1, 1)
        listbox.props.halign = Gtk.Align.CENTER
        listbox.add(row)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(3)
        # User feedback being the logo pops up and tells them what they
        # set a reminder for.
        run_type_label = Gtk.Label(label="Choose run type:")
        #run_type_label.props.halign = Gtk.Align.CENTER
        self.comboboxtext = Gtk.ComboBoxText()
        self.comboboxtext.append("standard","Standard")
        self.comboboxtext.append("tempo","Tempo")
        self.comboboxtext.append("interval","Interval")
        self.comboboxtext.append("hill","Hill")
        self.comboboxtext.connect("changed", self.update_save_button)
        # get info from comboboxtext.get_text()
        # halign is horizontal and valign is vertical aligning
        self.comboboxtext.props.halign = Gtk.Align.CENTER
        hbox.pack_start(run_type_label, True, True, 0)
        hbox.pack_start(self.comboboxtext, True, True, 0)

        listbox.add(row)
        
        
        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(3)
        # User feedback being the logo pops up and tells them what they
        # set a reminder for.
        self.save_event = Gtk.Button(label="Save Run!")

        """
        So this will make it so the user can't save a run before they 
        give info for each of the boxes.
        """
        #print(self.comboboxtext.get_active_text())
        if self.comboboxtext.get_active_text() is None:
            self.save_event.set_sensitive(False)

        self.save_event.connect("clicked", self.save_run)
        # halign is horizontal and valign is vertical aligning
        self.save_event.props.halign = Gtk.Align.CENTER
        hbox.pack_start(self.save_event, True, True, 0)

        listbox.add(row)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(3)
        # User feedback being the logo pops up and tells them what they
        # set a reminder for.

        self.inspirational_label = Gtk.Label()
        hbox.pack_start(self.inspirational_label, True, True, 0)
        listbox.add(row)


        self.notebook.append_page(
            self.run_page, Gtk.Label(label="Enter Run")
        )

        ################# analysis page ############################
        self.analysis_page = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.analysis_page)
        
        listbox = Gtk.ListBox()
        listbox.set_hexpand(True)
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(3)
        mileage_button = Gtk.Button(label="Daily Mileage Graph")
        mileage_button.connect("clicked", self.daily_mileage_graph)
        mileage_button.set_hexpand(True)

        hbox.pack_start(mileage_button, True, True, 0)
        listbox.add(row)


        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(3)
        type_button = Gtk.Button(label="Run Type Graph")
        type_button.connect("clicked", self.run_type_graph)
        type_button.set_hexpand(True)

        hbox.pack_start(type_button, True, True, 0)
        listbox.add(row)

        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(3)
        self.mileage_analysis_label = Gtk.Label()
        self.mileage_analysis()
        hbox.pack_end(self.mileage_analysis_label, True, True, 0)
        listbox.add(row)


        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)
        hbox.set_border_width(3)
        self.type_analysis_label = Gtk.Label()
        self.run_type_analysis()
        hbox.pack_end(self.type_analysis_label, True, True, 0)
        listbox.add(row)

                
        self.analysis_page.pack_start(listbox, True, True, 0)
        self.notebook.append_page(self.analysis_page, Gtk.Label(label="Analysis"))


        
        # Attaching everything to the grid, sorted from top to bottom
        # (button added, column, row, column span, row span)

        # adds the styling from the attached CSS file 'main.css'
        # color palette from https://photochrome.io/#running
        screen = Gdk.Screen.get_default()
        provider = Gtk.CssProvider()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(
            screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        # stuff needs to be lowercase for what it is in Gtk but without Gtk added in the name
        css_path = os.path.join(sys.path[0], "main.css")
        provider.load_from_path(css_path)

    def calendar_entry(self, widget):
        """
        What this will do is take the value and then
        put those values in a function that will make the date entry
        """
        dialog = CalDialog(self)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            chosen_dates = dialog.value
            year = chosen_dates[0]
            month = chosen_dates[1] + 1
            day = chosen_dates[2]
            # This will grab the year, month and day. Month seems
            # to be behind by 1 though... So add one to that
        # Take dialog values and put them into the text box for
        # the cal_entry
        self.user_entry.set_text(f"{month}/{day}/{year}")

        dialog.destroy()


    def nice_quotes(self):
        quotes = ["Tough runs don’t last; tough runners do",
                  "Run the first two-thirds of the race with your head and the last third with your heart",
                  "Do your feet hurt? It’s probably a mix of doing so much running and kicking so much ass! Keep it up!",
                  "What seems hard now will one day be your warm-up.",
                  "Pain is inevitable. Suffering is optional",
                  "There will come a day when I can no longer run. Today is not that day",
                  "A short run is better than no run",
                  "Run like you stole something",
                  "Run when you can, walk if you have to, crawl if you must; just never give up",
                  "Go as long as you can, and then take another step",
                  "It always seems impossible until it's done",
                  "Set your goals high, and don't stop until you get there",
                  "In order to succeed, you must first believe you can"]
        rand_quote = random.randint(0, len(quotes)-1)
        self.inspirational_label.set_text(quotes[rand_quote])
        self.inspirational_label.set_line_wrap(True)
        

    def save_run(self, widget):
        get_pace_min = self.minute_spin.get_value_as_int()
        get_pace_sec = self.second_spin.get_value_as_int()# get pace from input pace
        if get_pace_sec == 0:
            get_pace_sec = "00"
        get_pace = f"{get_pace_min}:{get_pace_sec} min/mile"
        get_distance_mile = self.distance_spin1.get_value_as_int()
        get_distance_dec = self.distance_spin2.get_value_as_int()
        get_distance = f"{get_distance_mile}.{get_distance_dec}" # get distance from input distance
        get_distance = float(get_distance)
        get_type = self.comboboxtext.get_active_text() # get type from combobox (WIP)
        get_date = self.user_entry.get_text() # get date from input
        if get_distance > 1.0:
            dist_string = "miles"
        elif get_distance < 1.0:
            dist_string = "mile"
        elif get_distance == 0.0:
            dist_string = "miles"

        sql = f"insert into {table} (date, distance, dist_type, pace, type) values (?,?,?,?,?)"


        try:
            cur.execute(sql, (f"{get_date}", get_distance, f"{dist_string}",
                              f"{get_pace}", f"{get_type}",))
            conn.commit()
            print("submited run!")
            self.distance_spin1.set_value(0)
            self.distance_spin2.set_value(0)
            self.minute_spin.set_value(0)
            self.second_spin.set_value(0)
            #self.nice_quotes()
            nocheefin = "notify-send 'Your run has been saved!' -i emblem-favorite"
            sp.call(nocheefin, shell=True)


        except Exception as e:
            print(f"Unable to submit to db, here is the error:\n{e}")
            
    def read_from_db(self):
        sql = f"select * from {table}"
        cur.execute(sql)
        
        records = cur.fetchall()
        runs = []
        for record in records:
            runs.append(record)

        weekly_runs = []

        self.last_week = self.last_week[1:]
        for run in runs:
            if run[0] < self.last_week:
                #print("Before this week:\n",run)
                pass
            elif run[0] > self.last_week:
                #print("During the week:\n",run[0])
                weekly_runs.append(run)

        weekly_runs = sorted(weekly_runs)

                
        return weekly_runs

    def mileage_analysis(self):#, widget):
        """
        Need to do a weekly filter for this as well
        """

        
        sql = f"select * from {table}"
        cur.execute(sql)
        records = cur.fetchall()
        run_length = []
        previous_mileage = []
        runs = []
        for record in records:
            runs.append(record)

        self.last_week = self.last_week[1:]

        self.two_weeks = self.two_weeks[1:]
        
        previous_mileage = []
        
        for run in runs:
            if runs is None:
                pass
            else:
                if self.two_weeks < run[0] < self.last_week:
                    previous_mileage.append(run[1])
                elif run[0] > self.last_week:
                    run_length.append(run[1])
        
        print("Run legnth:\n",run_length)
        running_sessions = len(run_length)
        week_mileage = sum(run_length)
        previous_mileage = sum(previous_mileage)
        print(week_mileage, previous_mileage)
        potential_mileage = running_sessions + previous_mileage
        """
        This will occupy a label on the screen and it will also show the users mileage
        """
        
        
        if week_mileage > potential_mileage:
            label_text = "You are increasing mileage a bit too quickly. Try and keep it to 1 mile per week per training session! "
            label_text += f"Your current mileage of {week_mileage} miles/week is {week_mileage-potential_mileage} miles "
            label_text += f"ahead of what you might want to think about doing."
            #print(label_text)
            

        elif week_mileage < previous_mileage:
            label_text = "It's good to take a rest week every once in a while, good job! But make sure to keep up the hard work!"
            #print(label_text)

        elif previous_mileage < week_mileage < potential_mileage:
            label_text = f"Good job steadily increasing your mileage! You've increased your mileage by {week_mileage-previous_mileage} miles this week, stay consistent and you'll see results!"
            #print(label_text)
        else:
            label_text = "Not enough runs to analysis progress!"
            
        self.mileage_analysis_label.set_text(f"{label_text}")
        self.mileage_analysis_label.set_line_wrap(True)

            
            
    def run_type_analysis(self):
        reg_runs = 0
        tempo_runs = 0
        interval_runs = 0
        hill_runs = 0
        sql = f"select * from {table}"
        cur.execute(sql)
        records = cur.fetchall()
        run_type_list = []
        runs = []
        for record in records:
            runs.append(record)
            #run_type_list.append(" ".join([str(x) for x in record]))

        self.last_week = self.last_week[1:]



        for run in runs:
            if runs is None:
                pass
            else:
                if run[0] < self.last_week:
                    pass
                elif run[0] > self.last_week:
                    run_type_list.append(run[4])

        #print(run_type_list)
                
        hard_runs = tempo_runs + interval_runs

        for run in run_type_list:
            if run == "Standard":
                reg_runs += 1
            elif run == "Tempo":
                tempo_runs += 1
            elif run == "Interval":
                interval_runs += 1
            elif run == "Hill":
                hill_runs += 1
        #print(reg_runs, tempo_runs, interval_runs, hill_runs)

        hard_runs = tempo_runs + interval_runs + hill_runs

                
        
        #print(run_type_list)
        
        if hard_runs > reg_runs:            
            analysis_output = "Be careful, don't do more hard runs than easier runs, that can lead to burnout."

        elif hard_runs == 0:
            analysis_output = "It is good to occasionally do hard runs even if you aren't training for an event"

        elif reg_runs > hard_runs and hard_runs != 0:
            analysis_output = "Great job keeping a balance of harder and easier runs. The balance of the two is very important to becoming a better runner"

        elif hard_runs == reg_runs:
            analysis_output = "Be careful, don't do more hard runs than easier runs, that can lead to burnout."

        self.type_analysis_label.set_text(f"{analysis_output}")
        self.type_analysis_label.set_line_wrap(True)

            
    def daily_mileage_graph(self, widget):
        import daily_mileage_graph
            
    def run_type_graph(self, widget):
        import run_type_graph

    def update_save_button(self, combo):
        self.save_event.set_sensitive(True)
    
    # used to dynamically update the weekly reminders
    def update_weekly_review(self):
        items = self.read_from_db()
        self.listbox_2.add(self.intro_label)
        x = 0
        for item in items:
            self.listbox_2.add(ListBoxRowWithData(item))
            x += 1
            if x % 4 == 0:
                self.listbox_2.add(ListBoxRowWithData("\n"))

        self.intro_page.add(self.listbox_2)
        self.listbox_2.show_all()

if __name__ == "__main__":
    window = running_calendar()
    window.connect("destroy", Gtk.main_quit)
    window.show_all()
    Gtk.main()
